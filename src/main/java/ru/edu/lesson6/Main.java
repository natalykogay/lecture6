package ru.edu.lesson6;

public class Main {
    public static void main(String[] args) {
        TravelService converter = new TravelService();
        GeoPosition geoPosition = new GeoPosition("43(35'07'')", "39(43'13'')");
        CityInfo city = new CityInfo("Sochi", geoPosition);
        converter.add(city);
        geoPosition = new GeoPosition("55(45'07'')", "37(36'56'')");
        city = new CityInfo("Moscow", geoPosition);
        converter.add(city);
        geoPosition = new GeoPosition("54(11'76'')", "37(37'09'')");
        CityInfo city2 = new CityInfo("Tyla", geoPosition);
        converter.add(city2);
        GeoPosition geoPosition3 = new GeoPosition("54(31'45'')", "36(16'31'')");
        CityInfo city3 = new CityInfo("Kalyga", geoPosition3);
        converter.add(city3);
        GeoPosition geoPosition4 = new GeoPosition("56(59'49'')", "40(58'17'')");
        CityInfo city4 = new CityInfo("Ivanovo", geoPosition4);
        converter.add(city4);

        System.out.println(converter.citiesNames());
        System.out.println(converter.getDistance("Ivanovo","Kalyga"));
        System.out.println(converter.getCitiesNear("Moscow",200));


    }
}
