package ru.edu.lesson6;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.*;

/**
 * Travel Service.
 */
public class TravelService {

    // do not change type
    private final List<CityInfo> cities = new ArrayList<>();

    /**
     * Append city info.
     *
     * @param cityInfo - city info
     * @throws IllegalArgumentException if city already exists
     */
    public void add(CityInfo cityInfo) {
        if (cities.contains(cityInfo)) {
            throw new IllegalArgumentException("City already exists");
        } else {
            cities.add(cityInfo);
        }
    }

    /**
     * remove city info.
     *
     * @param cityName - city name
     * @throws IllegalArgumentException if city doesn't exist
     */
    public void remove(String cityName) {
        CityInfo cityInfo = getCityInSource(cityName);
        if (cityInfo != null) {
            cities.remove(cityInfo);
        } else {
            throw new IllegalArgumentException("City doesn't exist");
        }
    }


    /**
     * Get cities names.
     */
    public List<String> citiesNames() {
        return cities.stream()
                .filter(cityInfo -> cityInfo.getName() != null)
                .map(CityInfo::getName)
                .collect(Collectors.toList());
    }

    /**
     * Get distance in kilometers between two cities.
     * https://www.kobzarev.com/programming/calculation-of-distances-between-cities-on-their-coordinates/
     *
     * @param srcCityName  - source city
     * @param destCityName - destination city
     * @throws IllegalArgumentException if source or destination city doesn't exist.
     */
    public int getDistance(String srcCityName, String destCityName) {
        CityInfo cityInfo = getCityInSource(srcCityName);
        CityInfo cityInfo2 = getCityInSource(destCityName);
        if (cityInfo == null || cityInfo2 == null) {
            throw new IllegalArgumentException("Source or destination city doesn't exist");
        }

        double cl1 = cos(cityInfo.getPosition().getLatitude());
        double cl2 = cos(cityInfo2.getPosition().getLatitude());
        double sl1 = sin(cityInfo.getPosition().getLatitude());
        double sl2 = sin(cityInfo2.getPosition().getLatitude());
        double delta = cityInfo.getPosition().getLongitude() - cityInfo2.getPosition().getLongitude();
        double cdelta = cos(delta);
        double sdelta = sin(delta);

        double y = sqrt(pow(cl2 * sdelta, 2) + pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
        double x = sl1 * sl2 + cl1 * cl2 * cdelta;

        double ad = atan2(y, x);
        double dist = ad * 6372795;

        return (int) (dist / 1000);
    }

    private CityInfo getCityInSource(String cityName) {
        return cities.stream()
                .filter(city -> city.getName().equals(cityName))
                .findAny()
                .orElse(null);
    }

    /**
     * Get all cities near current city in radius.
     *
     * @param cityName - city
     * @param radius   - radius in kilometers for search
     * @throws IllegalArgumentException if city with cityName city doesn't exist.
     */
    public List<String> getCitiesNear(String cityName, int radius) {
        CityInfo cityInfo = getCityInSource(cityName);
        if (cityInfo == null) {
            throw new IllegalArgumentException("City with cityName " + cityName + " doesn't exist");
        }
        List<String>  cityNames = cities.stream()
                .filter(city -> !(city.getName().equals(cityName)))
                .filter(city -> {
                    int km = getDistance(cityInfo.getName(), city.getName());
                    if (km <= radius) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .map(city -> city.getName())
                .collect(Collectors.toList());
        return cityNames;
    }
}
