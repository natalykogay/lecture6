package ru.edu.lesson6;

/**
 * Geo position.
 */
public class GeoPosition {

    /**
     * Широта в радианах.
     */
    private double latitude;

    /**
     * Долгота в радианах.
     */
    private double longitude;

    /**
     * Ctor.
     *
     * @param latitudeGradus  - latitude in gradus
     * @param longitudeGradus - longitude in gradus
     *                        Possible values: 55, 55(45'07''), 59(57'00'')
     */
    public GeoPosition(String latitudeGradus, String longitudeGradus) {
        double latitudeDegrees = parseDegrees(latitudeGradus);
        double longitudeDegrees = parseDegrees(longitudeGradus);
        double pi = 3.14159265358979;
        latitude = latitudeDegrees * pi / 180;
        longitude = longitudeDegrees * pi / 180;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private double parseDegrees(String degrees) {
        if (degrees.length() == 2) {
            return Double.parseDouble(degrees);
        }
        double degrees1;
        String[] array = degrees
                .replace("(", " ")
                .replace("'", " ")
                .replace(")", "")
                .split(" ");

        if (array.length == 3) {
            degrees1 = Double.parseDouble(array[0]) +
                    (Double.parseDouble(array[1]) / 60) +
                    (Double.parseDouble(array[2]) / 3600);
        } else {
            throw new IllegalArgumentException("Possible values: 55, 55(45'07''), 59(57'00'')");
        }
        return degrees1;
    }

    @Override

    public String toString() {
        return "GeoPosition{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
