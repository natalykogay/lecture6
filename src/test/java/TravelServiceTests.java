import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.edu.lesson6.CityInfo;
import ru.edu.lesson6.GeoPosition;
import ru.edu.lesson6.TravelService;

import java.util.Arrays;
import java.util.List;

public class TravelServiceTests {
    private GeoPosition geoPosition = new GeoPosition("43(35'07'')", "39(43'13'')");
    private CityInfo city = new CityInfo("Sochi", geoPosition);
    private TravelService converter = new TravelService();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Успешное выполнение метода add()
     */
    @Test
    public void addTest() {
        converter.add(city);
        List<String> name = converter.citiesNames();
        Assert.assertEquals(name.get(0),"Sochi");
    }

    /**
     * Успешное выполнение метода getDistance()
     */
    @Test
    public void getDistanceTest() {
        converter.add(city);
        GeoPosition geoPosition = new GeoPosition("55(45'07'')", "37(36'56'')");
        CityInfo city2 = new CityInfo("Moscow", geoPosition);
        converter.add(city2);
        int km = converter.getDistance("Moscow", "Sochi");
        Assert.assertEquals(km, 1361);
    }

    /**
     * Успешное выполнение метода getCitiesNear
     */
    @Test
    public void getCitiesNearTest() {
        converter.add(city);
        GeoPosition geoPosition1 = new GeoPosition("55(45'07'')", "37(36'56'')");
        CityInfo city1 = new CityInfo("Moscow", geoPosition1);
        converter.add(city1);
        GeoPosition geoPosition = new GeoPosition("54(11'76'')", "37(37'09'')");
        CityInfo city2 = new CityInfo("Tyla", geoPosition);
        converter.add(city2);
        GeoPosition geoPosition3 = new GeoPosition("54(31'45'')", "36(16'31'')");
        CityInfo city3 = new CityInfo("Kalyga", geoPosition3);
        converter.add(city3);
        GeoPosition geoPosition4 = new GeoPosition("56(59'49'')", "40(58'17'')");
        CityInfo city4 = new CityInfo("Ivanovo", geoPosition4);
        converter.add(city4);
        List<String> names = Arrays.asList("Tyla", "Kalyga");
        List<String> cityNames = converter.getCitiesNear("Moscow", 200);
        Assert.assertArrayEquals(names.toArray(), cityNames.toArray());
    }

    /**
     * Успешное выполнение метода getCitiesNear
     */
    @Test
    public void removeTest(){
        GeoPosition geoPosition1 = new GeoPosition("55(45'07'')", "37(36'56'')");
        CityInfo city1 = new CityInfo("Moscow", geoPosition1);
        converter.add(city1);
        converter.add(city);
       String[] names = {"Sochi"};
        converter.remove("Moscow");
        Assert.assertArrayEquals(names,converter.citiesNames().toArray());
    }

    /**
     * Неуспешное выполнение метода add()
     */
    @Test
    public void addNegativeTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("City already exists");
        converter.add(city);
        converter.add(city);
    }

    /**
     * Неуспешное выполнение метода remove()
     */
    @Test
    public void removeNegativeTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("City doesn't exist");
        converter.remove("Novgorod");
    }

    /**
     * Неуспешное выполнение метода getDistance()
     */
    @Test
    public void getDistanceNegativeTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Source or destination city doesn't exist");
        converter.getDistance("Prohladniy", "Sochi");
    }

    /**
     * Неуспешное выполнение метода getCitiesNear
     */
    @Test
    public void getCitiesNearNegativeTest() {
        String city = "Prohladniy";
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("City with cityName " + city + " doesn't exist");
        converter.getCitiesNear(city, 3);
    }
}
