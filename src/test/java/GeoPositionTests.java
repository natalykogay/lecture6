import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.edu.lesson6.GeoPosition;

public class GeoPositionTests {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Передаем верные параметры в конструктор
     */
    @Test
    public void positiveTest() {
        GeoPosition position = new GeoPosition("55","37");
        double value1 = position.getLatitude();
        double value2 = 55 * 3.14159265358979 / 180;
        Assert.assertTrue(value1==value2);
    }

    /**
     * Передаем неверные параметры в конструктор
     */
    @Test
    public void negativeTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Possible values: 55, 55(45'07''), 59(57'00'')");
        GeoPosition position = new GeoPosition("55(34)","37,12'12");
    }
}
